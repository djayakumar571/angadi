package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OrderDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int orderId;
	private String email;
	private String fullName;
	private String location;
	@Column(length = 1000)
	private String fullAddress;
	private String city;
	private String pincode;
	private String phoneNumber;
	public OrderDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrderDetails(int orderId, String email, String fullName, String location, String fullAddress, String city,
			String pincode, String phoneNumber) {
		super();
		this.orderId = orderId;
		this.email = email;
		this.fullName = fullName;
		this.location = location;
		this.fullAddress = fullAddress;
		this.city = city;
		this.pincode = pincode;
		this.phoneNumber = phoneNumber;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getfullName() {
		return fullName;
	}
	public void setfullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getFullAddress() {
		return fullAddress;
	}
	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Override
	public String toString() {
		return "OrderDetails [orderId=" + orderId + ", email=" + email + ", fullName=" + fullName + ", location="
				+ location + ", fullAddress=" + fullAddress + ", city=" + city + ", pincode=" + pincode
				+ ", phoneNumber=" + phoneNumber + "]";
	}
	
	
}
