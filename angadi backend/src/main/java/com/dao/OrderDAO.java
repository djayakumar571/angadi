package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.OrderDetails;


@Service
public class OrderDAO {
	
	@Autowired
	OrderRepository orderRepo;
	
	public List<OrderDetails> getAllOrders(){
		return orderRepo.findAll();
	}
	public void deleteOrderDetails(Integer orderId) {
		orderRepo.deleteById(orderId);
	}

	public OrderDetails getorderDetailsById(Integer orderId) {
		
		return orderRepo.findById(orderId).get();
	}
	
	public List<OrderDetails> getorderDetails(boolean isSingeorderCheckout, Integer orderId) {
	
		if(isSingeorderCheckout && orderId != 0) {
			List<OrderDetails> list = new ArrayList<OrderDetails>();
			OrderDetails order = orderRepo.findById(orderId).get();
			list.add(order);
			return list;
		}
		
		return null;
	}
	public OrderDetails addNewOrder(OrderDetails order) {
		// TODO Auto-generated method stub
		return orderRepo.save(order);
	}
	public OrderDetails getOrderDetailsById(Integer orderId) {
		// TODO Auto-generated method stub
		return orderRepo.findById(orderId).get();
	}
}
