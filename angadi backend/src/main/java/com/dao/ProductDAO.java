package com.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Product;

@Service
public class ProductDAO {
	
	@Autowired
	private ProductRepository productRepo;
	
	public Product addNewProduct(Product product) {
		return productRepo.save(product);		
	}
	
	public List<Product> getAllProducts(){
		return productRepo.findAll();
	}
	public void deleteProductDetails(Integer productId) {
		productRepo.deleteById(productId);
	}

	public Product getProductDetailsById(Integer productId) {
		
		return productRepo.findById(productId).get();
	}
	
	public List<Product> getProductDetails(boolean isSingeProductCheckout, Integer productId) {
	
		if(isSingeProductCheckout && productId != 0) {
			List<Product> list = new ArrayList<Product>();
			Product product = productRepo.findById(productId).get();
			list.add(product);
			return list;
		}
		
		return null;
	}
}
