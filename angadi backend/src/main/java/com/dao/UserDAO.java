package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDAO {
	
	private UserRepository userRepo;
	
	public UserDAO(UserRepository userRepo){
		super();
		this.userRepo = userRepo;
	}
	
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}
	
	public User getUserByName(String firstName){
		return userRepo.findByName(firstName);
	}
	
	public User getUserByEmail(String email){
		return userRepo.findByEmailId(email);
	}
	public User registerUser(User usr){
		BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
		String encryptPass = bcpe.encode(usr.getPassword());
		usr.setPassword(encryptPass);
		return userRepo.save(usr);
	}
	
	public User updateUser(User usr){
		return userRepo.save(usr);
	}
	
	public void deleteUserByEmail(String email){
		userRepo.deleteById(email);
	}
	
	public String forgotPassword(String email){
		
			Optional<User> login = userRepo.findById(email);
			
			User mail = login.get();
			return sendMail(email, "OTP Details", " Dear " + mail.getFirstName() + " , \r\n" + " Please use the below One Time Password (OTP) to reset your password. \r\n"+" One Time Password (OTP): " + generatePassword() + " \r\n If you are unable to change the password, please click on 'Forgot Password' and continue with the same process again. \r\n Wish you all the best!"
					+ "\r\n Regards \r\n Angadi.com");
		
		
	}
	
	@Autowired
	private JavaMailSender javaMailSender;
	public String sendMail(String to,String subject,String body){
		SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
		
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(body);
		simpleMailMessage.setTo(to);
		javaMailSender.send(simpleMailMessage);
		return "Mail Sent";
	}
	public String generatePassword(){
		String passString="0123456789";
		StringBuilder sBuilder=new StringBuilder(6);
		for(int i=0;i<6;i++){
			int index=(int)(passString.length()*Math.random());
			sBuilder.append(passString.charAt(index));
		}
		return sBuilder.toString();
	}
}
