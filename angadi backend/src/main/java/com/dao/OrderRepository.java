package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.model.OrderDetails;
import com.model.User;

@Repository
public interface OrderRepository extends JpaRepository<OrderDetails, Integer> {
	@Query("from OrderDetails where productName = :name")
	User findByName(@Param("name") String productName);
}
