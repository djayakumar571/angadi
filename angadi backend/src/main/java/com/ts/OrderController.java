package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.OrderDAO;
import com.model.OrderDetails;


@RestController
public class OrderController {
	
	@Autowired
	private OrderDAO orderDao;
	
	@PostMapping("/checkOut")
	public ResponseEntity<OrderDetails> registerUser(@RequestBody OrderDetails Order){
		OrderDetails addOrder = orderDao.addNewOrder(Order);
		return new ResponseEntity<>(addOrder, HttpStatus.CREATED);
	}
	
	@GetMapping("getAllOrders")
	public List<OrderDetails> getAllOrders(){
		return orderDao.getAllOrders();
	}
	
	
	@GetMapping({"/getOrderDetailsById/{OrderId}"})
	public OrderDetails getOrderDetailsById(@PathVariable("OrderId") Integer OrderId) {
		
		return orderDao.getOrderDetailsById(OrderId);
		
	}
	
	@DeleteMapping({"/deleteOrderDetails/{OrderId}"})
	public void deleteOrderDetailes(@PathVariable("OrderId") Integer OrderId) {
		orderDao.deleteOrderDetails(OrderId);
	}
	
	
}
