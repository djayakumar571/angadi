package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;

@RestController
public class UserController {
	
	@Autowired
	UserDAO userDAO;
	
	@GetMapping("getAllUsers")
	public List<User> getAllUsers(){
		return userDAO.getAllUsers();
	}
	
	@GetMapping("getUserByFirstName/{name}")
	public User getUserByFirstName(@PathVariable("name")String firstName){
		return userDAO.getUserByName(firstName);
	}
	
	@GetMapping("usrLogin/{emailId}/{password}")
	public String uLogin(@PathVariable("email") String emailId, @PathVariable("password") String password){
		User usr = userDAO.getUserByEmail(emailId);
		if(usr != null){
			BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
			if(bcpe.matches(password, usr.getPassword())){
				return "Logined Succesfully";
			}
		}
		return "UnSuccesfully";
	}

	@PostMapping("/registerUser")
	public ResponseEntity<User> registerUser(@RequestBody User usr){
		User registeredUser = userDAO.registerUser(usr);
		return new ResponseEntity<>(registeredUser, HttpStatus.CREATED);
	}
	
	@PostMapping("/forgotPassword")
	public  String forgotPassword(@RequestParam("email") String email){
		return userDAO.forgotPassword(email);
	}
}
