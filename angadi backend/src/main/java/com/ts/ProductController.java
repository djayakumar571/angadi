package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;



@RestController
public class ProductController{
	
	@Autowired
	private ProductDAO productDao;
	
	@PostMapping("/addnewProduct")
	public ResponseEntity<Product> registerProduct(@RequestBody Product product){
		Product addProduct = productDao.addNewProduct(product);
		return new ResponseEntity<>(addProduct, HttpStatus.CREATED);
	}
	
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts(){
		return productDao.getAllProducts();
	}
	
	
	@GetMapping({"/getProductDetailsById/{productId}"})
	public Product getProductDetailsById(@PathVariable("productId") Integer productId) {
		
		return productDao.getProductDetailsById(productId);
		
	}
	
	@DeleteMapping({"/deleteProductDetails/{productId}"})
	public void deleteProductDetailes(@PathVariable("productId") Integer productId) {
		productDao.deleteProductDetails(productId);
	}
	
	@GetMapping({"/getProductDetails/{isSingeProductCheckout}/{productId}"})
	public List<Product> getProductDetails(@PathVariable(name="isSingeProductCheckout") boolean isSingeProductCheckout,
										@PathVariable(name= "productId") Integer productId) {
		
		return productDao.getProductDetails(isSingeProductCheckout, productId);
		
		
	}
}